﻿#language: ru

@tree

Функционал: Проверка расчета итогового количества в Заказе

Контекст:

Дано Я запускаю сценарий открытия TestClient или подключаю уже существующий


Сценарий: _001 Подготовка данных

	И Подготовить данные для Заказа

Сценарий: _002 Проверка расчета итогового количества

	И Открыть новый заказ
	И Заполнить шапку документа Заказ

	* Добавить 2 товара в заказ

		И в таблице "Товары" я нажимаю на кнопку с именем 'ТоварыДобавить'
		И в таблице "Товары" из выпадающего списка с именем "ТоварыТовар" я выбираю по строке 'товар 1'
		И в таблице "Товары" в поле с именем 'ТоварыКоличество' я ввожу текст '32'
		И в таблице "Товары" я завершаю редактирование строки

		И в таблице "Товары" я нажимаю на кнопку с именем 'ТоварыДобавить'
		И в таблице "Товары" из выпадающего списка с именем "ТоварыТовар" я выбираю по строке 'товар 2'
		И в таблице "Товары" в поле с именем 'ТоварыКоличество' я ввожу текст '68'
		И в таблице "Товары" я завершаю редактирование строки

	* Добавить услугу в заказ

		И в таблице "Товары" я нажимаю на кнопку с именем 'ТоварыДобавить'
		И в таблице "Товары" из выпадающего списка с именем "ТоварыТовар" я выбираю по строке 'услуга'
		И в таблице "Товары" я завершаю редактирование строки

	* Проверить итоговое количество

		И элемент формы с именем 'ТоварыИтогКоличество' стал равен '101'


