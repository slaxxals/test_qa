﻿#language: ru

@tree

Функционал: Тестирование отчетов

Как <Роль> я хочу
<описание функционала> 
чтобы <бизнес-эффект> 

Контекст:
	Дано Я запускаю сценарий открытия TestClient или подключаю уже существующий

Сценарий: _001 подготовительный - загрузка данных (тестирование отчетов)

	// чистая база с загрузкой данных
	Когда экспорт основных данных
	И экспорт документов закупок
	И экспорт документов продаж
	И экспорт документов возврата
	* Проведение всех документов кроме возврата
		И я выполняю код встроенного языка на сервере
		"""bsl
			Documents.GoodsReceipt.FindByNumber( 1 ).GetObject( ).Write( DocumentWriteMode.Posting );
			Documents.GoodsReceipt.FindByNumber( 4 ).GetObject( ).Write( DocumentWriteMode.Posting );
			
			Documents.PurchaseInvoice.FindByNumber( 1 ).GetObject( ).Write( DocumentWriteMode.Posting );
			Documents.PurchaseInvoice.FindByNumber( 2 ).GetObject( ).Write( DocumentWriteMode.Posting );

			Documents.PurchaseOrder.FindByNumber( 1 ).GetObject( ).Write( DocumentWriteMode.Posting );
			Documents.PurchaseOrder.FindByNumber( 2 ).GetObject( ).Write( DocumentWriteMode.Posting );
			Documents.PurchaseOrder.FindByNumber( 3 ).GetObject( ).Write( DocumentWriteMode.Posting );
			
			Documents.PurchaseOrderClosing.FindByNumber( 1 ).GetObject( ).Write( DocumentWriteMode.Posting );

			Documents.PurchaseReturn.FindByNumber( 1 ).GetObject( ).Write( DocumentWriteMode.Posting );
			Documents.PurchaseReturn.FindByNumber( 2 ).GetObject( ).Write( DocumentWriteMode.Posting );

			Documents.SalesInvoice.FindByNumber( 1 ).GetObject( ).Write( DocumentWriteMode.Posting );
		"""

Сценарий: _002 Проверка отображения возвратов от покупателя в отчете Продажи

	* Проверка отчета до проведения возврата от покупателя

		И Я открываю навигационную ссылку "e1cib/app/Report.D2001_Sales"
		Когда открылось окно 'D2001 Продажи'
		И я нажимаю на кнопку с именем 'FormGenerate'
		И Табличный документ "Result" равен макету "МакетОтчетаПродажи_ДоВозврата"

	* Проведение возврата от покупателя
		
		И я выполняю код встроенного языка на сервере
		"""bsl
			Documents.SalesReturn.FindByNumber( 1 ).GetObject( ).Write( DocumentWriteMode.Posting );
		"""
	* Проверка отчета после проведения возврата от покупателя
		
		И я нажимаю на кнопку с именем 'FormGenerate'
		И Табличный документ "Result" равен макету "МакетОтчетаПродажи_ПослеВозврата"
